import {Field, ID, InputType} from "@nestjs/graphql";
import {Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@InputType()
export class UpdateUsersDto{
    @Field(()=>ID)
    id:number;


    @Field({nullable:true})

    email:string;

    @Field({nullable:true})

    name:string;
}