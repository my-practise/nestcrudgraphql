import {Field, ID, InputType} from "@nestjs/graphql";
import {Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@InputType()

export class CreateUsersDto{
    @Field()
    email:string;

    @Field({nullable:true})
    name:string;
}