import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {UserEntity} from "../entities/user.entity";
import {DeleteResult, Repository} from "typeorm";
import {CreateUsersDto} from "../dto/create-users.dto";
import {UpdateUsersDto} from "../dto/update-users.dto";

@Injectable()
export class UserService{
    constructor(@InjectRepository(UserEntity)
                private readonly userRepository: Repository<UserEntity>) {
    }

    async createUser(dto:CreateUsersDto) : Promise<UserEntity>{
        return await this.userRepository.save({...dto});

    }

    async getOneUser(id:number):Promise<UserEntity>{
        return await this.userRepository.findOne({id});
    }

    async getAllUsers():Promise<UserEntity[]>{
        return await this.userRepository.find();
    }

    async delete(id:number):Promise<number>{
         await this.userRepository.delete({id});
         return id;
    }

    async update(dto:UpdateUsersDto):Promise<UserEntity>{
         await this.userRepository.update({id:dto.id},{...dto});

         return this.getOneUser(dto.id);
    }
}