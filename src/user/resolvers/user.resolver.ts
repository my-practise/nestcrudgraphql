import {Args, Mutation, Query, Resolver} from "@nestjs/graphql";
import {UserService} from "../services/user.service";
import {UserEntity} from "../entities/user.entity";
import {CreateUsersDto} from "../dto/create-users.dto";
import {UpdateUsersDto} from "../dto/update-users.dto";


@Resolver()
export class UserResolver{
constructor(private readonly userService:UserService) {
}

@Mutation(()=>UserEntity)//otpravka
    async createUser(@Args('createUser') dto:CreateUsersDto):Promise<UserEntity>{
    return await this.userService.createUser(dto);
}

@Mutation(()=>UserEntity)//otpravka
async updateUser(@Args('updateUser') dto:UpdateUsersDto):Promise<UserEntity>{
    return await this.userService.update(dto);
}

@Mutation(()=>Number) //otpravka
async deleteUser(@Args('id')id:number):Promise<number>{
    return await this.userService.delete(id);

}
@Query(()=>UserEntity)//polushenie
    async getOneUser(@Args('id') id:number):Promise<UserEntity>{
    return await this.userService.getOneUser(id);
}

@Query(()=>[UserEntity])//polushenie
async getAllUsers():Promise<UserEntity[]>{
    return await this.userService.getAllUsers();
}

}