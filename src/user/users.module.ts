import {Module} from "@nestjs/common";
import {UserEntity} from "./entities/user.entity";
import {TypeOrmModule} from "@nestjs/typeorm";
import {UserService} from "./services/user.service";
import {UserResolver} from "./resolvers/user.resolver";

@Module({
imports:[TypeOrmModule.forFeature([UserEntity])],
    providers:[UserService,UserResolver]
})

export class UsersModule{

}